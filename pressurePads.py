import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)    #Pin number 7
GPIO.setup(17,GPIO.IN)   #Pin number 11
GPIO.setup(27,GPIO.IN)   #Pin number 13
GPIO.setup(22,GPIO.IN)   #Pin number 15

prev_input = 0    #For pin number 7 (GPIO 4)
prev_input2 = 0   #For pin number 11 (GPIO 17)
prev_input3 = 0   #For pin number 13 (GPIO 27)
prev_input4 = 0   #For pin number 15 (GPIO 22)

try:
    while True:
        #reading
        input = GPIO.input(4)
        input2 = GPIO.input(17)
        input3 = GPIO.input(27)
        input4 = GPIO.input(22)
        
        #if the last reading was low and this one high, alert
        if ((not prev_input) and input):
            print("P1 Under Pressure")
        
        if((not prev_input2) and input2):
            print("P2 Under Pressure")
            
        if((not prev_input3) and input3):
            print("P3 Under Pressure")
        
        if((not prev_input4) and input4):
            print("P4 Under Pressure")
        
        #update previous input
        prev_input = input
        prev_input2 = input2
        prev_input3 = input3
        prev_input4 = input4
        
        #pause
        time.sleep(0.10)
        
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()